var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var header = require('gulp-header');

var paths = {
    theme : './web/app/themes/flatsome-child/'
};

var banner = ['/*',
    'Theme Name: Lingerie Daniels',
    'Description: New Lingerie Daniels theme with Eshop 2014',
    'Author: Vincent & Simon Peters',
    'Author URI: http: //unde.fined.io',
    'Template: flatsome',
    'Version: 1.8',
    '*/',
    ''].join('\n');

gulp.task('sass', function() {
    gulp.src(paths.theme + 'src/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(header(banner))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.theme));
});

gulp.task('watch', function () {
    gulp.watch(paths.theme + 'src/scss/**/*.scss', ['sass']);
});

gulp.task('compile', ['sass']);

gulp.task('default', ['sass', 'watch']);