<?php
session_start(); 
/*///////////////////////////////////////////////////////////////////////
Part of the code from the book 
Building Findable Websites: Web Standards, SEO, and Beyond
by Aarron Walter (aarron@buildingfindablewebsites.com)
http://buildingfindablewebsites.com

Distrbuted under Creative Commons license
http://creativecommons.org/licenses/by-sa/3.0/us/
///////////////////////////////////////////////////////////////////////*/
if(!isset($_SESSION['admin'])){
		$response['status'] = 'validation';
		$response['error'] = 'Geen admin meer, log opnieuw in.';
	echo json_encode($response);
	die();
}

function storeAddress(){


	$response['status'] = '';
	$response['error'] = '';
	
	// Validation
	if(!$_GET['email']){ 
		$response['status'] = 'validation';
		$response['error'] = 'Email is niet ingevult.';
		return json_encode($response);
	} 
	if(!$_GET['fname']){
		$response['status'] = 'validation';
		$response['error'] = 'Voornaam is niet ingevult.';
		return json_encode($response);
	} 
	if(!$_GET['lname']){ 
		$response['status'] = 'validation';
		$response['error'] = 'Achternaam is niet ingevult.';
		return json_encode($response);
	 } 

	$merge_vars = array('FNAME'=>$_GET['fname'], 'LNAME'=>$_GET['lname']);

	if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_GET['email'])) {
		$response['status'] = 'validation';
		$response['error'] = 'Gegeven email is geen geldig email.';
		return json_encode($response);
	}

	require_once('MCAPI.class.php');
	// grab an API Key from http://admin.mailchimp.com/account/api/
	$api = new MCAPI('7e338d12dd26afa744fb40246861a7a5-us4');
	
	// grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
	// Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
	$list_id = "adfea289a4";

	if($api->listSubscribe($list_id, $_GET['email'], $merge_vars, html, false ,false ,true , true ) === true) {
		$response['success'] = true;
		$response['error'] = 'success';
		return json_encode($response);
	}else{
		// An error ocurred, return error message	
		$response['status'] = 'email';
		$response['error'] = 'Error: ' . $api->errorMessage;
		return json_encode($response);	
	}

	
}

// If being called via ajax, autorun the function
if($_GET['ajax']){ echo storeAddress(); }
?>