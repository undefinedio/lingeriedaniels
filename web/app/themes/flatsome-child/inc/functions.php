<?php

/**
 * Debugging echo and die
 * @param $var
 */
function dd($var)
{
    echo "<pre>";
    var_dump($var);
    die();
}

/**
 * Adds zeros in front of number
 * @param $number
 * @param int $amount of zeros to max add
 * @return string
 */
function prependZeros($number, $amount = 2)
{
    if (strlen((string)$number) < $amount) {
        $toAdd = $amount - strlen((string)$number);
        for ($i = 1; $i <= $toAdd; $i++) {
            $number = "0" . $number;
        }
    }
    return $number;

}

/**
 * Find out what season to show, parameters are set from options pane
 * @return string
 */
function getSeason()
{
    $startSummer = DateTime::createFromFormat('Ymd',
        date("Y")
        . prependZeros(get_field('zomer_start_maand', 'option'))
        . prependZeros(get_field('zomer_start_dag', 'option')));
    $startWinter = DateTime::createFromFormat('Ymd',
        date("Y")
        . prependZeros(get_field('winter_start_maand', 'option'))
        . prependZeros(get_field('winter_start_dag', 'option')));
    $today = DateTime::createFromFormat('Ymd', date("Ymd"));


    if ($today <= $startSummer || $today > $startWinter) {
        $season = "winter";
    } else {
        $season = "zomer";
    }

    return $season;
}

/**
 * Get the collection year (difference between two winter collection in one year)
 * @return int
 */
function getYear()
{
    $startSummer = DateTime::createFromFormat('Ymd',
        date("Y")
        . prependZeros(get_field('zomer_start_maand', 'option'))
        . prependZeros(get_field('zomer_start_dag', 'option')));
    $today = DateTime::createFromFormat('Ymd', date("Ymd"));

    if ($today <= $startSummer) {
        $year = date("Y") - 1;
    } else {
        $year = date("Y");
    }

    return (integer)$year;
}

function new_excerpt_more($more)
{
    return '… <a class="read-more" href="' . get_permalink(get_the_ID()) . '">' . __('Lees meer', 'daniels') . '</a>';
}

//add_filter( 'excerpt_more', 'new_excerpt_more' );


function truncate($string, $length = 100, $append = "&hellip;")
{
    $string = trim($string);

    if (strlen($string) > $length) {
        $string = wordwrap($string, $length);
        $string = explode("\n", $string, 2);
        $string = $string[0] . $append;
    }

    return $string;
}

/**
 * Generates the posts markup.
 *
 * @since  0.9.4
 * @param  array $args
 * @return string|array The HTML for the random posts.
 */
function rpwe_get_recent_events($args = array()) {

    // Set up a default, empty variable.
    $html = '';

    // Merge the input arguments and the defaults.
    $args = wp_parse_args($args, rpwe_get_default_args());

    // Extract the array to allow easy use of variables.
    extract($args);

    // Allow devs to hook in stuff before the loop.
    do_action('rpwe_before_loop');

    // Display the default style of the plugin.
    if ($args['styles_default'] === true) {
        rpwe_custom_styles();
    }

    // If the default style is disabled then use the custom css if it's not empty.
    if ($args['styles_default'] === false && !empty($args['css'])) {
        echo '<style>' . $args['css'] . '</style>';
    }

    // Get the posts query.
    $posts = rpwe_get_posts($args);

    if ($posts->have_posts()) :

        $html = '<div ' . (!empty($args['cssID']) ? 'id="' . sanitize_html_class($args['cssID']) . '"' : '') . ' class="rpwe-block ' . (!empty($args['css_class']) ? '' . sanitize_html_class($args['css_class']) . '' : '') . '">';

        $html .= '<ul class="rpwe-ul">';

        while ($posts->have_posts()) : $posts->the_post();

            // Thumbnails
            $thumb_id = get_post_thumbnail_id(); // Get the featured image id.
            $img_url = wp_get_attachment_url($thumb_id); // Get img URL.

            // Display the image url and crop using the resizer.
            $image = rpwe_resize($img_url, $args['thumb_width'], $args['thumb_height'], true);

            // Start recent posts markup.
            $html .= '<li class="rpwe-li rpwe-clearfix">';

            if ($args['thumb']) :

                // Check if post has post thumbnail.
                if (has_post_thumbnail()) :
                    $html .= '<a class="rpwe-img" href="' . esc_url(get_permalink()) . '"  rel="bookmark">';
                    if ($image) :
                        $html .= '<img class="' . esc_attr($args['thumb_align']) . ' rpwe-thumb" src="' . esc_url($image) . '" alt="' . esc_attr(get_the_title()) . '">';
                    else :
                        $html .= get_the_post_thumbnail(get_the_ID(),
                            array($args['thumb_width'], $args['thumb_height']),
                            array(
                                'class' => $args['thumb_align'] . ' rpwe-thumb the-post-thumbnail',
                                'alt' => esc_attr(get_the_title())
                            )
                        );
                    endif;
                    $html .= '</a>';

                // If no post thumbnail found, check if Get The Image plugin exist and display the image.
                elseif (function_exists('get_the_image')) :
                    $html .= get_the_image(array(
                        'height' => (int)$args['thumb_height'],
                        'width' => (int)$args['thumb_width'],
                        'image_class' => esc_attr($args['thumb_align']) . ' rpwe-thumb get-the-image',
                        'image_scan' => true,
                        'echo' => false,
                        'default_image' => esc_url($args['thumb_default'])
                    ));

                // Display default image.
                elseif (!empty($args['thumb_default'])) :
                    $html .= sprintf('<a class="rpwe-img" href="%1$s" rel="bookmark"><img class="%2$s rpwe-thumb rpwe-default-thumb" src="%3$s" alt="%4$s" width="%5$s" height="%6$s"></a>',
                        esc_url(get_permalink()),
                        esc_attr($args['thumb_align']),
                        esc_url($args['thumb_default']),
                        esc_attr(get_the_title()),
                        (int)$args['thumb_width'],
                        (int)$args['thumb_height']
                    );

                endif;

            endif;

            $html .= '<h3 class="rpwe-title"><a href="' . esc_url(get_permalink()) . '" title="' . sprintf(esc_attr__('Permalink to %s', 'rpwe'), the_title_attribute('echo=0')) . '" rel="bookmark">' . esc_attr(get_the_title()) . '</a></h3>';
            $date = DateTime::createFromFormat('Ymd', get_field('datum'));
            $endDate = DateTime::createFromFormat('Ymd', get_field('eind_datum'));
            $sameDate = true;

            $evenementDate = '';

            if ($endDate) {
                $sameDate = ($date->format('M') == $endDate->format('M')) ? true : false;
            }

            $evenementDate = $date->format('d');

            if ($endDate && $sameDate) {
                $evenementDate .= '-' . $endDate->format('d');
            }

            $evenementDate .= ' ' . $date->format('M');

            if ($endDate && !$sameDate) {
                $evenementDate .= '-' . $endDate->format('d');
                $evenementDate .= ' ' . $endDate->format('M');
            }

            if ($args['date']) :
                $date = get_the_date();
                $html .= '<time class="evenement-date rpwe-time published" datetime="' . esc_html(get_the_date('c')) . '">' . esc_html($evenementDate) . '</time>';
            endif;

            if ($args['excerpt']) :
                $html .= '<div class="rpwe-summary">';
                $html .= wp_trim_words(apply_filters('rpwe_excerpt', get_the_excerpt()), $args['length'], ' &hellip;');
                if ($args['readmore']) :
                    $html .= '<a href="' . esc_url(get_permalink()) . '" class="more-link">' . $args['readmore_text'] . '</a>';
                endif;
                $html .= '</div>';
            endif;

            $html .= '</li>';

        endwhile;

        $html .= '</ul>';

        $html .= '</div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ -->';

    endif;

    // Restore original Post Data.
    wp_reset_postdata();

    // Allow devs to hook in stuff after the loop.
    do_action('rpwe_after_loop');

    // Return the  posts markup.
    return $args['before'] . apply_filters('rpwe_markup', $html) . $args['after'];

}

/* INCLUDE CUSTOM WIDGETS */
include_once('widgets/cadeaubon-widget.php'); // Load Widget Recent Posts
include_once('widgets/facebook-widget.php'); // Load Widget Recent Posts
include_once('widgets/events-widget.php'); // Load Widget Recent Posts