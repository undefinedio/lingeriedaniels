<?php

// Register Custom Post Type
function custom_post_type()
{

    /**
     * Tips
     */

    $labels = [
        'name'               => _x('Tips', 'Post Type General Name', 'text_domain'),
        'singular_name'      => _x('Tip', 'Post Type Singular Name', 'text_domain'),
        'menu_name'          => __('Tips', 'text_domain'),
        'parent_item_colon'  => __('Parent Item:', 'text_domain'),
        'all_items'          => __('All Items', 'text_domain'),
        'view_item'          => __('View Item', 'text_domain'),
        'add_new_item'       => __('Add New Item', 'text_domain'),
        'add_new'            => __('Add New', 'text_domain'),
        'edit_item'          => __('Edit Item', 'text_domain'),
        'update_item'        => __('Update Item', 'text_domain'),
        'search_items'       => __('Search Item', 'text_domain'),
        'not_found'          => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    ];
    $args = [
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => ['slug' => 'tips'],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'taxonomies'         => ['category', 'post_tag'],
        'supports'           => ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments']
    ];

    register_post_type('tips', $args);

    /**
     * Evenementen
     */
    $labels = [
        'name'               => _x('Evenementen', 'Post Type General Name', 'text_domain'),
        'singular_name'      => _x('Evenement', 'Post Type Singular Name', 'text_domain'),
        'menu_name'          => __('Evenementen', 'text_domain'),
        'parent_item_colon'  => __('Parent Item:', 'text_domain'),
        'all_items'          => __('All Items', 'text_domain'),
        'view_item'          => __('View Item', 'text_domain'),
        'add_new_item'       => __('Add New Item', 'text_domain'),
        'add_new'            => __('Add New', 'text_domain'),
        'edit_item'          => __('Edit Item', 'text_domain'),
        'update_item'        => __('Update Item', 'text_domain'),
        'search_items'       => __('Search Item', 'text_domain'),
        'not_found'          => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    ];

    $args = [
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => ['slug' => 'evenementen'],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'taxonomies'         => ['category', 'post_tag'],
        'supports'           => ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments']
    ];

    register_post_type('evenementen', $args);

    /**
     * De Merken
     */
    $labels = [
        'name'               => _x('Merken', 'Post Type General Name', 'text_domain'),
        'singular_name'      => _x('Merk', 'Post Type Singular Name', 'text_domain'),
        'menu_name'          => __('Merken', 'text_domain'),
        'parent_item_colon'  => __('Parent Item:', 'text_domain'),
        'all_items'          => __('All Items', 'text_domain'),
        'view_item'          => __('View Item', 'text_domain'),
        'add_new_item'       => __('Add New Item', 'text_domain'),
        'add_new'            => __('Add New', 'text_domain'),
        'edit_item'          => __('Edit Item', 'text_domain'),
        'update_item'        => __('Update Item', 'text_domain'),
        'search_items'       => __('Search Item', 'text_domain'),
        'not_found'          => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    ];

    $args = [
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => ['slug' => 'merken'],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'taxonomies'         => [],
        'supports'           => ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments']
    ];
    register_post_type('merken', $args);
}

// Hook into the 'init' action
add_action('init', 'custom_post_type', 0);

/**
 * Custom taxonomies
 */

function type_taxonomy()
{

    $labels = [
        'name'                       => 'Types',
        'singular_name'              => 'Type',
        'menu_name'                  => 'Types',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'search_items'               => 'Search Items',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used items',
        'not_found'                  => 'Not Found',
    ];
    $args = [
        'labels'            => $labels,
        'hierarchical'      => true,
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud'     => true,
    ];
    register_taxonomy('type', ['collecties', 'merken'], $args);
}

// Hook into the 'init' action
add_action('init', 'type_taxonomy', 0);

add_theme_support('post-thumbnails');
add_image_size('merk-cover', 700, 320, true);
add_image_size('collectie-thumbnail', 320);

