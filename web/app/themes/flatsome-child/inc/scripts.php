<?php

/**
* add custom child theme javascript
*/
function flatsome_child_scripts() {

    wp_enqueue_script( 'dropDown-filler', get_template_directory_uri() . '/js/dropdownfiller.js', array(), '1.0.0', true );
    wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/js/lightbox.min.js', array('jquery'), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'flatsome_child_scripts' );