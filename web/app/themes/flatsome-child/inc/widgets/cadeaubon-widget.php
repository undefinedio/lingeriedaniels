<?php 
/**
 * Adds Cadeaubon_Widget.
 */

function register_cadeaubon_widget() {
    register_widget( 'Cadeaubon_Widget' );
}
add_action( 'widgets_init', 'register_cadeaubon_widget' );

class Cadeaubon_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'cadeaubon_widget', // Base ID
			'Cadeaubon', // Name
			array( 'description' => __( 'Cadeaubon', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;
		
?>
        <div class="cadeaubon-widget">
            <h3 class="widget-title">Schenk een cadeaubon</h3>
            <a href="<?php echo get_permalink(2896); ?>" class="cadeaubon-widget-img">
                <h3><?php
                    if ( ! empty( $title ) )
                        echo $title;
                    ?>
                </h3>
            </a>
        </div>
		<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Complete the look', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
} // class Foo_Widget