<?php
/**
 * @package flatsome
 */

global $flatsome_opt;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header text-center">
        <h1 class="entry-title"><?php the_title(); ?></h1>

        <div class="entry-meta">
            <?php
            printf(__('<span class="meta-author">Geschreven door %7$s.</span> Gepost op <time class="entry-date" datetime="%3$s">%4$s</time>', 'flatsome'),
                esc_url(get_permalink()),
                esc_attr(get_the_time()),
                esc_attr(get_the_date('c')),
                esc_html(get_the_date()),
                esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                esc_attr(sprintf(__('View all posts by %s', 'flatsome'), get_the_author())),
                get_the_author()
            );
            ?>
        </div>
        <!-- .entry-meta -->
    </header>
    <!-- .entry-header -->

    <?php if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it. ?>
        <div class="entry-image">
            <?php if ($flatsome_opt['blog_parallax']) { ?>
            <div class="parallax_img" style="overflow:hidden">
                <div class="parallax_img_inner" data-velocity="0.15"><?php } ?>
                    <?php the_post_thumbnail('large'); ?>
                    <?php if ($flatsome_opt['blog_parallax']) { ?></div>
            </div><?php } ?>
        </div>
    <?php } ?>

    <div class="entry-content">
        <?php the_content(); ?>
        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'flatsome'),
            'after' => '</div>',
        ));
        ?>
    </div>
    <!-- .entry-content -->

    <footer class="entry-meta">
        <div class="date-container">
            <div class="single-date"><img class="icon-calendar" src="<?php echo get_stylesheet_directory_uri(); ?>/img/calendar.svg" alt=""/><?php echo get_the_date(); ?></div>
        </div>
        <?php if (isset($flatsome_opt['blog_share']) && $flatsome_opt['blog_share']) {
            // SHARE ICONS
            echo '<div class="blog-share">';
            echo do_shortcode('[share]');
            echo '</div>';
        } ?>
    </footer>
    <!-- .entry-meta -->
    <?php
    echo do_shortcode('[block id="nieuwsbrief"]');
    echo do_shortcode('[block id="nieuwsbrief-blog"]');
    ?>

    <?php flatsome_content_nav('nav-below'); ?>
</article><!-- #post-## -->
