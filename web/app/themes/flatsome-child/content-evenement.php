<?php
/**
 * @package flatsome
 */
global $flatsome_opt;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header text-center">
        <?php include('evenement-date.php'); ?>
        <h1 class="entry-title"><?php the_title(); ?></h1>

        <div class="entry-meta">
            <?php
            printf( __( '<span class="meta-author">Geschreven door %7$s.</span> Gepost op <time class="entry-date" datetime="%3$s">%4$s</time>', 'flatsome' ),
                esc_url( get_permalink() ),
                esc_attr( get_the_time() ),
                esc_attr( get_the_date( 'c' ) ),
                esc_html( get_the_date() ),
                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                esc_attr( sprintf( __( 'View all posts by %s', 'flatsome' ), get_the_author() ) ),
                get_the_author()
            );
            ?>
        </div>
        <!-- .entry-meta -->
    </header>
    <!-- .entry-header -->

    <?php if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it. ?>
        <div class="entry-image">
            <?php if ($flatsome_opt['blog_parallax']) { ?>
            <div class="parallax_img" style="overflow:hidden">
                <div class="parallax_img_inner" data-velocity="0.15"><?php } ?>
                    <?php the_post_thumbnail('large'); ?>
                    <?php if ($flatsome_opt['blog_parallax']) { ?></div>
            </div><?php } ?>
        </div>
    <?php } ?>

    <div class="entry-content">
        <?php the_content(); ?>
        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'flatsome'),
            'after' => '</div>',
        ));
        ?>
    </div>
    <!-- .entry-content -->

    <footer class="entry-meta">
        <div class="date-container">
            <div class="evenement-date">evenementdatum: </div>
            <?php include('evenement-date.php'); ?>
        </div>
    <?php if (isset($flatsome_opt['blog_share']) && $flatsome_opt['blog_share']) {
        // SHARE ICONS
        echo '<div class="blog-share">';
        echo do_shortcode('[share]');
        echo '</div>';
    } ?>


    </footer>
    <!-- .entry-meta -->

    <?php /* if(isset($flatsome_opt['blog_author_box']) && $flatsome_opt['blog_author_box']) { ?>
        <div class="author-box">
            <div class="row">
                <div class="large-2 small-3 columns">
                    <?php

                    $user = get_the_author_meta('ID');
                    echo get_avatar($user,90);

                    ?>

                </div>
                <div class="large-10 small-9 columns">
                    <h4 class="author-name"><?php _e('by','flatsome') ?> <?php echo get_the_author_meta('display_name');?></h4>

                    <?php if(get_the_author_meta('yim')){?>
                        <p class="author-title"><?php echo get_the_author_meta('yim'); ?></p>
                    <?php }?>

                    <p class="author-desc"><?php echo get_the_author_meta('user_description');?></p>

                    <?php if(get_the_author_meta('aim')){?>
                        <p class="author-twitter"><a href="http://twitter.com/<?php echo get_the_author_meta('aim');?>"><?php echo get_the_author_meta('aim');?></a></p>
                    <?php }?>
                </div>
            </div>
        </div>
    <?php } */
    ?>

    <?php
    echo do_shortcode('[block id="nieuwsbrief"]');
    echo do_shortcode('[block id="nieuwsbrief-blog"]');
    ?>

    <?php flatsome_content_nav('nav-below'); ?>
</article><!-- #post-## -->

