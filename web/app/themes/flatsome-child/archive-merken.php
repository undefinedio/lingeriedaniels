<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package flatsome
 */

get_header();
if (!isset($flatsome_opt['blog_layout'])) {
    $flatsome_opt['blog_layout'] = '';
}
?>

<?php // ADD BLOG HEADER IF SET
if (isset($flatsome_opt['blog_header'])) {
    echo do_shortcode($flatsome_opt['blog_header']);
}
?>
    <div class="page-wrapper page-<?php if ($flatsome_opt['blog_layout']) {
        echo $flatsome_opt['blog_layout'];
    } else {
        echo 'right-sidebar';
    } ?>">
        <div class="row">

            <?php if ($flatsome_opt['blog_layout'] == 'left-sidebar') {
                echo '<div id="content" class="large-9 right columns" role="main">';
            } else if ($flatsome_opt['blog_layout'] == 'right-sidebar') {
                echo '<div id="content" class="large-9 left columns" role="main">';
            } else if ($flatsome_opt['blog_layout'] == 'no-sidebar' && $flatsome_opt['blog_style'] == 'blog-pinterest') {
                echo '<div id="content" class="large-12 columns" role="main">';
            } else if ($flatsome_opt['blog_layout'] == 'no-sidebar') {
                echo '<div id="content" class="large-10 columns large-offset-1" role="main">';
            } else {
                echo '<div id="content" class="large-9 left columns" role="main">';
            }
            ?>

            <h2 style="text-align: center;">Mooie lingerie vind je terug in al onze merken</h2>

            <div class="page-inner merken-overview">

                <?php /* Start the Loop */

                $args = array(
                    'posts_per_page' => -1,
                    'post_type'      => 'merken',
                    'post_status'    => 'publish'
                );
                $the_query = new WP_Query($args);

                //var_dump($the_query);
                ?>

                <?php if ($the_query->have_posts()) { ?>

                    <?php
                    /* Include the Post-Format-specific template for the content.
                     * If you want to overload this in a child theme then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    while ($the_query->have_posts()) : $the_query->the_post(); ?>

                        <?php
                        $ID = get_the_ID();
                        $logo = get_field('logo_wit');

                        $collectie = get_field('collectie', $ID);
                        if (have_rows('collectie')):
                            $bgImage = false;
                            while (have_rows('collectie')) : the_row();
                                if (!$bgImage) {
                                    $bgImage = wp_get_attachment_image_src(get_sub_field('foto'), 'collectie-thumbnail');
                                    $bgImage = $bgImage[0];
                                }
                            endwhile;
                            ?>
                            <div class="large-4 columns">
                                <a href="<?= get_the_permalink($ID); ?>">
                                    <article id="post-<?= $ID; ?>" <?php post_class($ID); ?>>
                                        <div class="entry-image merk-image"
                                             style="background-image: url(<?= $bgImage; ?>); background-size: cover;">
                                            <div class="overlay"></div>

                                            <span class="helper"></span>
                                            <img src="<?= $logo; ?>" alt="Logo"/>
                                        </div>
                                    </article>
                                </a>
                            </div>
                            <?php
                        endif;
                    endwhile; ?>
                <?php } ?>
            </div>
            <!-- .page-inner -->

        </div>
        <!-- #content -->

        <div class="large-3 columns left">
            <?php if ($flatsome_opt['blog_layout'] == 'left-sidebar' || $flatsome_opt['blog_layout'] == 'right-sidebar') {
                get_sidebar();
            } ?>
        </div>
        <!-- end sidebar -->

    </div><!-- end row -->
    </div><!-- end page-wrapper -->

<?php get_footer(); ?>