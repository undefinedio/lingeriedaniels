<?php
/**
 * Query to get all the brands
 */
$the_query = new WP_Query( array('post_type' => 'merken') );
/**
 * Loop all brands and put them inside an multi array
 */
if ( $the_query->have_posts() ) {
	$results = array();
	while ( $the_query->have_posts() ) : $the_query->the_post();
	    $item = array('name' => get_the_title(), 'url' => get_permalink());
		array_push($results, $item);
	endwhile;
	wp_reset_postdata();
}

/**
 * Bootstrap all this data to an javascript object
 */
?>
<!-- JS bootstrap-->
<script>
	<?php echo "var json_merken = " . json_encode($results); ?>
</script>