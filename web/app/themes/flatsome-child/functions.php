<?php
//die();
include_once('inc/functions.php');

include_once('inc/shortcodes/custom_posts.php');
include_once('inc/sidebars.php');

include_once('inc/scripts.php');

if (function_exists('acf_add_options_page')) {

    $page = acf_add_options_page(array(
        'page_title' => 'Seizoen Opties',
        'menu_title' => 'Seizoen opties',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));
}
