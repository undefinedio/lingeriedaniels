<?php
/**
 * @package flatsome
 */

global $flatsome_opt, $page;
?>



<div class="large-4 columns blog-pinterest-style">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
            <div class="entry-image">

                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('medium'); ?>
                </a>

            </div>
        <?php } ?>




        <div class="entry-content">
            <?php if ('post' == get_post_type()) : // Hide category and tag text for pages on Search ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(', ', 'flatsome'));
                ?>
                <span class="cat-links">
				<?php printf(__('%1$s', 'flatsome'), $categories_list); ?>
			</span>


            <?php endif; // End if 'post' == get_post_type() ?>
            <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>

<!--            <div class="tx-div small"></div>-->
            <p><?php// echo truncate(get_field('beschrijving', $page_id), 275); ?></p>

            <?php if ('post' == get_post_type()) : ?>
                <?php if (!post_password_required() && (comments_open() || '0' != get_comments_number())) : ?>
                    <span
                        class="comments-link"><?php comments_popup_link(__('Leave a comment', 'flatsome'), __('<strong>1</strong> Comment', 'flatsome'), __('<strong>%</strong> Comments', 'flatsome')); ?></span>
                <?php endif; ?>
            <?php endif; ?>


        </div>
        <!-- .entry-content -->

    </article>
    <!-- #post-## -->
</div>

