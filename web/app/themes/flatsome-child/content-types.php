<?php
/**
 * @package flatsome
 */

global $flatsome_opt, $page;
$url = get_the_permalink();
?>


<div class="blog-list-style">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="row">
            <div class="large-12 columns">
                <h3 class="section-title "><span><?php the_title(); ?></span> <a href="<?php the_permalink(); ?>">Bekijk
                        de <?= getSeason(); ?> collectie</a></h3>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="entry-image merk-image">

                    <a href="<?= $url; ?>">
                        <span class="helper"></span><?php the_post_thumbnail('medium'); ?>
                    </a>

                </div>
            </div>
            <?php



            $collectie = get_posts(array(
                'post_type' => 'collecties',
                'posts_per_page' => 3,
                'post_status' => 'publish',
                "orderby" => 'rand',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'merk',
                        'value' => get_the_ID(),
                        'compare' => '='
                    ),
                    array(
                        'key' => 'seizoen',
                        'value' => getSeason(),
                        'compare' => '='
                    ),
                    array(
                        'key' => 'jaar',
                        'value' => getYear(),
                        'compare' => '='
                    )
                )
            ));

            foreach ($collectie as $post) : setup_postdata($post); ?>
                <div class="large-3 columns">
                    <a href="<?= $url.'#'.$post->ID; ?>"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></a>
                </div>
            <?php endforeach;
            wp_reset_postdata();?>

        </div>


    </article>
    <!-- #post-## -->
</div><!-- .blog-list-style -->

