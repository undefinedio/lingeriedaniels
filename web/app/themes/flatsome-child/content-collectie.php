<div class="large-4 columns blog-pinterest-style merk-photo">
    <a href="<?= wp_get_attachment_url(get_sub_field('foto')); ?>" data-id="<?php the_ID(); ?>" data-lightbox="lightbox"
       data-title="<?php the_title(); ?>"
       class="js-openlightbox">
        <article>
            <div class="entry-image">
                <?= wp_get_attachment_image(get_sub_field('foto'), 'collectie-thumbnail'); ?>
            </div>

            <div class="entry-content">
                <h3 class="entry-title"><?php the_sub_field('naam'); ?></h3>
                <?php if (get_sub_field('type')) {
                    $term = get_sub_field('type'); ?>
                    <p><?php echo $term->name; ?> </p>
                <?php } ?>
            </div>
            <!-- .entry-content -->
        </article>
    </a>
    <!-- #post-## -->
</div>
