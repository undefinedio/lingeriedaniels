<?php
/**
 * The Template for displaying all single posts.
 *
 * @package flatsome
 */

get_header();

global $flatsome_opt;
if (!isset($flatsome_opt['blog_layout'])) {
    $flatsome_opt['blog_layout'] = '';
}
?>

<?php // ADD BLOG HEADER IF SET
if (isset($flatsome_opt['blog_header'])) {
    echo do_shortcode($flatsome_opt['blog_header']);
}
?>

    <div class="page-wrapper ">
        <div class="row">

            <div class="large-12 columns">
                <h1 class="text-center"><?php the_title(); ?></h1>
            </div>

            <div id="content" class="large-12 left columns" role="main">

                <div class="merk-info large-9 large-offset-2 clearfix">
                    <?php

                    global $wp_query;

                    $page_id = $wp_query->post->ID;
                    $url = wp_get_attachment_url(get_post_thumbnail_id($page_id));
                    ?>

                    <div class="logo  small-6 small-offset-3 large-3 large-offset-0 left columns">
                        <img src="<?= $url; ?>" alt=""/>

                    </div>

                    <div class="large-8 left columns"><?php the_field('beschrijving', $page_id); ?></div>

                </div>

                <!-- #secondary -->
                <div class="page-inner">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="blog-list-style">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="row collectie-wrapper">
                                    <?php
                                    if (have_rows('collectie')):
                                        while (have_rows('collectie')) : the_row();
                                            get_template_part('content', 'collectie');
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                            </article>
                        </div>
                    <?php endwhile; // end of the loop. ?>
                </div>
                <!-- .page-inner -->
            </div>
            <!-- #content -->
        </div>
        <!-- end row -->
    </div><!-- end page-wrapper -->

    <script>
        jQuery(document).ready(function ($) {
            imagesLoaded(document.querySelector('.page-inner'), function (instance, container) {
                var $container = $(".page-inner");
                // initialize
                $container.packery({
                    itemSelector: ".columns",
                    gutter: 0
                });
                $container.packery('layout');
            });
        });
    </script>

<?php get_footer(); ?>