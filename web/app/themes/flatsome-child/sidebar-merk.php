<?php
/**
 * The Sidebar containing the main widget areas .
 *
 * @package flatsome
 */
?>
<div id="secondary" class="widget-area" role="complementary">
    <?php

    global $wp_query;

    $page_id = $wp_query->post->ID;
    $url = wp_get_attachment_url(get_post_thumbnail_id($page_id));
    ?>
    <div class="logo">
        <img src="<?= $url; ?>" alt=""/>
    </div>
    <p><?php the_field('beschrijving', $page_id); ?></p>


</div><!-- #secondary -->
<script>
    jQuery(document).ready(function ($) {
        var $container = $(".collectie-wrapper");
        $container.packery({
            itemSelector: ".columns",
            gutter: 0
        });
        imagesLoaded(document.querySelector('.page-inner'), function (instance, container) {
            $container.packery('layout');
            if(window.location.hash ){
                var openUp = mystring = window.location.hash.replace('#', '');
                $('.dash'+openUp +" a").click();

            }
        });
        $('.js-openlightbox').on('click', function(){
            window.location.hash = $(this).data('id');
        });

        $('.js-filter').on('click', function () {

            var tax = $(this).data('tax');

            $(".js-filter").removeClass('active');
            $(this).addClass('active');

            if(tax == "alles"){
                $('.collectie-wrapper .columns').show();
            }else{
                $('.collectie-wrapper .columns.'+tax).show();
                $('.collectie-wrapper .columns:not(.'+tax+')').hide();
            }

            $container.packery('layout');
        });

    });
</script>
