<div class="blog-list-style">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
				<div class="large-4 columns">
					<div class="entry-image">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail( 'medium' ); ?>
						</a>
					</div>
				</div>
			<?php } ?>

			<div class="large-8 columns">

				<div class="entry-content">
					<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
						<?php
						/* translators: used between list items, there is a space after the comma */
						$categories_list = get_the_category_list( __( ', ', 'flatsome' ) );
						?>
						<span class="cat-links">
				<?php printf( __( '%1$s', 'flatsome' ), $categories_list ); ?>
			</span>

						<?php
						/* translators: used between list items, there is a space after the comma */
						$tags_list = get_the_tag_list( '', __( ', ', 'flatsome' ) );
						if ( $tags_list ) :
							?>
							<span class="sep"> | </span>
							<span class="tags-links">
				<?php printf( __( 'Tagged %1$s', 'flatsome' ), $tags_list ); ?>
			</span>
						<?php endif; // End if $tags_list ?>
					<?php endif; // End if 'post' == get_post_type() ?>
					<h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
					</h3>
                    <div class="blog-date large">
                        <span class="blog-date-day"><?php echo get_the_time( 'd', get_the_ID() ); ?></span>
                        <span class="blog-date-month"><?php echo get_the_time( 'M', get_the_ID() ); ?></span>
                    </div>
					<?php
					$short = get_field( 'korte_tekst' );
					if ( $short ) {
						echo $short;
					} else {
						the_excerpt();
					} ?>
                    <a class="blog-read-more" href="<?php the_permalink(); ?>">Lees Meer</a>

					<?php if ( 'post' == get_post_type() ) : ?>
						<div class="entry-meta">
							<?php flatsome_posted_on(); ?>  <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
								<span
									class="comments-link right"><?php comments_popup_link( __( 'Leave a comment', 'flatsome' ), __( '<strong>1</strong> Comment', 'flatsome' ), __( '<strong>%</strong> Comments', 'flatsome' ) ); ?></span>
							<?php endif; ?>
						</div><!-- .entry-meta -->
					<?php endif; ?>
				</div>
				<!-- .entry-content -->
			</div>
			<!-- .large-8 -->
		</div>
		<!-- .row -->

	</article>
	<!-- #post-## -->
</div><!-- .blog-list-style -->