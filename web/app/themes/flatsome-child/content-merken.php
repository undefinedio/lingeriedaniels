<?php
/**
 * @package flatsome
 */

global $flatsome_opt, $page;

?>
<div class="large-3 columns">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-image merk-image">

            <a href="<?= get_the_permalink(); ?>">
                <span class="helper"></span><?php the_post_thumbnail('medium'); ?>
            </a>

        </div>
    </article>
</div>


