<?php
$date = DateTime::createFromFormat('Ymd', get_field('datum'));
$endDate = DateTime::createFromFormat('Ymd', get_field('eind_datum'));
$sameDate = true;

if ($endDate) {
    $sameDate = ($date->format('M') == $endDate->format('M')) ? true : false;
}
?>
<div class="evenement-date <?php if(!$sameDate) echo "multi"; ?>">

    <span class="evenement-date-day"><?= $date->format('d'); ?></span>
    <?php if ($endDate && $sameDate) {
        ?>-<span class="evenement-date-day"><?= $endDate->format('d'); ?></span>
    <?php } ?>

    <span class="evenement-date-month"><?= $date->format('M'); ?></span>

    <?php if ($endDate && !$sameDate) {
        ?>-<br><span class="evenement-date-day"><?= $endDate->format('d'); ?></span>
        <span class="evenement-date-month"><?= $endDate->format('M'); ?></span>
    <?php } ?>

</div>