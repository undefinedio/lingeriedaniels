<?php
/**
 * @package flatsome
 */


global $flatsome_opt, $page;

$date = DateTime::createFromFormat('Ymd', get_field('datum'));
$endDate = DateTime::createFromFormat('Ymd', get_field('eind_datum'));
$sameDate = true;
if ($endDate) {
    $sameDate = ($date->format('M') == $endDate->format('M')) ? true : false;
}


?>



<div class="large-4 columns blog-pinterest-style">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it. ?>
            <div class="entry-image">

                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail('medium'); ?>
                </a>

            </div>
        <?php } ?>




        <div class="entry-content">
            <?php if ('post' == get_post_type()) : // Hide category and tag text for pages on Search ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(', ', 'flatsome'));
                ?>
                <span class="cat-links">
				<?php printf(__('%1$s', 'flatsome'), $categories_list); ?>
			</span>


            <?php endif; // End if 'post' == get_post_type() ?>

            <div class="date <?php if (!$sameDate) {
                echo "multi";
            } ?>">

                <span class="post-date-day"><?= $date->format('d'); ?></span>
                <?php if ($endDate && $sameDate) {
                    ?>-<span class="post-date-day"><?= $endDate->format('d'); ?></span>
                <?php } ?>

                <span class="post-date-month"><?= $date->format('M'); ?></span>

                <?php if ($endDate && !$sameDate) {
                    ?>-<br><span class="post-date-day"><?= $endDate->format('d'); ?></span>
                    <span class="post-date-month"><?= $endDate->format('M'); ?></span>
                <?php } ?>

            </div>

            <h3 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h3>

            <p><?php echo short_excerpt(24); ?></p>

            <?php if ('post' == get_post_type()) : ?>
                <?php if (!post_password_required() && (comments_open() || '0' != get_comments_number())) : ?>
                    <span
                        class="comments-link"><?php comments_popup_link(__('Leave a comment', 'flatsome'), __('<strong>1</strong> Comment', 'flatsome'), __('<strong>%</strong> Comments', 'flatsome')); ?></span>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', __(', ', 'flatsome'));
            if ($tags_list) :
                ?>
                <span class="sep"> | </span>
                <span class="tags-links">
				<?php printf(__('Tagged %1$s', 'flatsome'), $tags_list); ?>
			</span>
            <?php endif; // End if $tags_list ?>

        </div>

        <a class="evenement-btn" href="<?php the_permalink(); ?>">Bekijk Het Event</a>
        <!-- .entry-content -->

    </article>
    <!-- #post-## -->
</div>

