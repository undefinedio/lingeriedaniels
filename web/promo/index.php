<?php
session_start();
//get name from URL and filter
if (isset($_GET['name'])) {
    $name = filter_var($_GET['name'], FILTER_SANITIZE_STRING);
}

//check if name is empty
if (!empty($name)) {
    //encrypt string
    $key = "PffRgePwGx8hSGUgp2u0AEBOGPKq2zwz4iM0kk2Yz0ODsZtnPsRgNQQzyiW7IoG";
    $_SESSION['name'] = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $name, MCRYPT_MODE_CBC, md5(md5($key))));
    header('Location: promobon.php');
}