<?php
session_start();

error_reporting(E_ALL);
ini_set("display_errors", 1);

function toAscii($str, $replace = [], $delimiter = '-')
{
    if (!empty($replace)) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

if (isset($_SESSION['name'])) {
    $name = filter_var($_SESSION['name'], FILTER_SANITIZE_STRING);
}

if (!empty($name)) {
    //decrypt from session
    $key = "PffRgePwGx8hSGUgp2u0AEBOGPKq2zwz4iM0kk2Yz0ODsZtnPsRgNQQzyiW7IoG";
    $name = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($name), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

    $imageName = toAscii($name) . ".jpg";

    if (!file_exists("bon/" . $imageName)) {
        $code = uniqid();
        $date = date("d-m-Y");

        //get image
        $rImg = ImageCreateFromJPEG("bon.jpg");
        //define color
        $cor = imagecolorallocate($rImg, 0, 0, 0);

        //write on image
        imagestring($rImg, 4, 210, 230, urldecode($name), $cor);
        imagestring($rImg, 4, 210, 310, urldecode($code), $cor);
        imagestring($rImg, 4, 210, 390, urldecode($date), $cor);
        //Header output
        //header('Content-type: image/jpeg');
        imagejpeg($rImg, 'bon/' . $imageName, 100);
    }
}

?>
<html>
<head>
    <title>Cadeaubon Lingerie Daniels</title>
    <style type="text/css" media="print">
        .print {
            visibility: hidden;
        }
    </style>
</head>
<body>
<img src="bon/<?php echo $imageName; ?>" alt="Lingerie Daniels cadeaubon"/>

<form><input class="print" id="print" type="button" value=" Print deze bon af"
             onclick="window.print();return false;"/></form>
</body>
</html>