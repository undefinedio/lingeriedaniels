set :stage, :production

# Extended Server Syntax
# ======================
server '178.18.136.201', user: 'lingerieda', roles: %w{web app db}

set :deploy_to, -> { "~/app" }
set :tmp_dir, '~/tmp'
set :branch, :master

set :wpcli_remote_url, 'lingeriedaniels.be'
set :wpcli_local_url, 'dev.lingeriedaniels.be'

fetch(:default_env).merge!(wp_env: :production)