$user = 'stagingfined'
$url = 'lingeriedaniels.stage.fined.io'

server 'server.fined.io', user: $user, roles: %w{web app db}

set :deploy_to, -> { "/home/#$user/subdomains/lingeriedaniels" }
set :tmp_dir, "/home/#$user/tmp"
set :branch, :'develop'

set :wpcli_remote_url, "lingeriedaniels.stage.fined.io"
set :wpcli_local_url, "dev.lingeriedaniels.be"

fetch(:default_env).merge!(wp_env: :staging)

