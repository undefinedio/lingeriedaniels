set :application, 'lingeriedaniels'
set :repo_url, 'git@gitlab.com:undefinedio/lingeriedaniels.git'

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, -> { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master

set :deploy_to, -> { "~/www/" }

# Use :debug for more verbose output when troubleshooting
set :log_level, :info

set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

namespace :npm do
  task :install do
    on roles :app do
      within release_path do
        execute :npm, :install
      end
    end
  end
end

namespace :bower do
  task :install do
    on roles :app do
      within release_path do
        with path: "#{release_path}/node_modules/.bin:$PATH" do
          execute :bower, :install, "--config.interactive=false"
        end
      end
    end
  end
end

namespace :gulp do
  task :compile do
    on roles :app do
      within release_path do
        with path: "#{release_path}/node_modules/.bin:$PATH" do
          execute :gulp, :compile
        end
      end
    end
  end
end

after "deploy:updating", "npm:install"
#after "deploy:updating", "bower:install"
after "deploy:updating", "gulp:compile"